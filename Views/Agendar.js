import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  TextInput,
  TouchableOpacity,
  ListView,
  AsyncStorage,
  Image,
  ImageBackground
} from 'react-native';
import Modal from "react-native-modal";
import * as global from '../global/const'

import InputAgendar from '../InputAgendar'
import ItemsAgendar from '../itemAgendar'
import ItemsAgendar2 from '../itemAgendar2'

const days = [
    {
      label: 'Lunes',
      value: 1,
      id:1
    },
    {
      label: 'Martes',
      value: 2,
      id:2
    },
    {
        label: 'Miercoles',
        value: 3,
        id:3
    },
    {
        label: 'Jueves',
        value: 4,
        id:4
    },
    {
        label: 'Viernes',
        value: 5,
        id:5
    },
    {
        label: 'Sabado',
        value: 6,
        id:6
    },
    {
        label: 'Domingo',
        value: 7,
        id:7
    }
];

const citys = [
    {
        label: 'Valparaiso',
        //value: 1,
    },
    {
        label: 'Viña del mar',
        //value: 2,
    },
];
export default class Agendar extends Component {
    constructor(props){
        super(props)

        const ds=new ListView.DataSource({rowHasChanged:(r1,r2)=>r1 !== r2});

        this.state = {
            dataSource:ds.cloneWithRows([]),
            items:[],
            hours:[],
            isModalVisible: false,
            date:'',
            finishTime:'',
            day:1,
            city:'Valparaiso',
            isChecked:true,
            lunes:false,
            martes:false,
            miercoles:false,
            jueves:false,
            viernes:false,
            sabado:false,
            domingo:false,
            title:'',
            id:''
          };
          this.sumFunc = this.sumFunc.bind(this)
          this.sendData = this.sendData.bind(this)
          this.setDay = this.setDay.bind(this)
          this.onChangeFinishTime = this.onChangeFinishTime.bind(this)
          this.handleRemoveItem = this.handleRemoveItem.bind(this)
          this.handleAddItems = this.handleAddItems.bind(this)
          this.handleState = this.handleState.bind(this)
          this.onChangeDate = this.onChangeDate.bind(this)
          this.onChangeCity = this.onChangeCity.bind(this)
          this.onChangeSwitch = this.onChangeSwitch.bind(this)
          this.onChangeLu = this.onChangeLu.bind(this)
          this.onChangeMa = this.onChangeMa.bind(this)
          this.onChangeMi = this.onChangeMi.bind(this)
          this.onChangeJu = this.onChangeJu.bind(this)
          this.onChangeVi = this.onChangeVi.bind(this)
          this.onChangeSa = this.onChangeSa.bind(this)
          this.onChangeDo = this.onChangeDo.bind(this)
    }

    onChangeLu(){
        this.setState({isChecked:!this.state.lunes})
    }
    onChangeMa(){
        this.setState({isChecked:!this.state.martes})
    }
    onChangeMi(){
        this.setState({isChecked:!this.state.miercoles})
    }
    onChangeJu(){
        this.setState({isChecked:!this.state.jueves})
    }
    onChangeVi(){
        this.setState({isChecked:!this.state.viernes})
    }
    onChangeSa(){
        this.setState({isChecked:!this.state.sabado})
    }
    onChangeDo(){
        this.setState({isChecked:!this.state.domingo})
    }
    onChangeSwitch(){
        this.setState({isChecked:!this.state.isChecked})
    }
    

    setDay(day){
        this.setState({day})
    }

    _getToken = async () => {
        try{
            const token = await AsyncStorage.getItem('id_token');
            //alert(token)
            return token;
            //this.props.navigation.navigate('App');
        }
        catch (error) {
            console.log('Error con el token: ' + error.message);
        }
    };

    componentWillMount(){
        this.getData()
    }

    onChangeDate(date){
        this.setState({
            date:date
        })
    }
    onChangeFinishTime(finishTime){
        this.setState({finishTime})
    }
    onChangeCity(city){
        this.setState({city:city})
    }

    

    getData(){

        const tok=this._getToken().then((value) => {
            if (value !== null) {
                fetch(global.API_GET_SHHEDULE,{
                    method:'GET',
                    headers:{
                        'x-auth-token':value,
                    }

                })
                .then((response) => response.json())
                .then((responseJson) => {
                    this.setState({
                        items:responseJson
                    })
                    
                    console.log("respuesta get: ",responseJson)
                    console.log("items: ", this.state.items);
                    
                    /*for(let j=0; j < responseJson.length ; j++ ){
                        for(let i=0; i < responseJson[j].scheduleAvailability.length ; i++ ){
                            this.setState({
                                ...this.state,
                                id: responseJson[j].scheduleAvailability[i]._id
                            })
                            //console.log("asd", responseJson[j].scheduleAvailability[i]._id);
                        }
                    }    
                    console.log("dasfa",this.state.id)
        */

                })
            }
        })

    }

    
    sendData ( )  {
        //this.getData()
        const tok=this._getToken().then((value) => {
            if (value !== null) {
                //console.log("city: ",this.state.city)
                //console.log("token: ", value);
                //console.log("date: ", this.state.date);
                //console.log("finish: ", Number(this.state.finishTime));
                //console.log("day: ", this.state.day);
                //console.log("dataSource: ", this.state.dataSource);
                //console.log("items: ", this.state.items);
                //const inicio =this.state.date.split(":")
                //const fin = this.state.finishTime.split(":")

                fetch(global.API_SEND_SCHEDULE, {
                  method:'POST',
                    headers: {
                      'Content-Type':'application/json',
                      'x-auth-token': value
                      },
                    body: JSON.stringify({

                        "city": this.state.city,
                        "title": "...",
                        "daysSelected": {
                            "monday": false,
                            "tuesday": true,
                            "wednesday": true,
                            "thursday": true,
                            "friday": false,
                            "saturday": false,
                            "sunday": false
                        },
                        "repeat": {
                            "every": 1,
                            "repeatType": 0
                        },
                        "endDate": "2018-11-07T00:00:00.000Z",
                        "scheduleAvailability": [
                            {
                            "fromDate": this.state.date,
                            "toDate": this.state.finishTime
                            }
                        ]
                        /*"day":this.state.day,
                        "fromHour": Number(this.state.date) ,
                        "toHour": Number(this.state.finishTime),
                        "city": this.state.city,
                        "isChecked": "true",*/
                    })
                })
                .then((response) => response.json())
                .then((responseJson) => {
                    console.log("respuesta send data: ",responseJson)
                    this.getData()

                })
                .catch((error) => {
                    console.log(error);
                })
                
      
          }
        });
    }

    deleteData(id){
        const tok=this._getToken().then((value) => {
            if (value !== null) {
              
                fetch(global.API_DELETE_SCHEDULE+id, {
                  method:'DELETE',
                    headers: {
                      'x-auth-token': value,
                      'Content-Type':'application/json'
                      },

                })
              /*  .then((response) => response.json())
                .then((responseJson) => {
                    console.log("respuesta delete: ",responseJson)

                })
                .catch((error) => {
                    console.log(error);
                })
                */
      
          }
        });
        this.getData()
    }

    handleState(items,dataSource, obj={}){
        this.setState({
            items,
            dataSource: this.state.dataSource.cloneWithRows(dataSource),
            ...obj
        })
        //AsyncStorage.setItem('items',JSON.stringify(items));
    }

    handleAddItems(){
        //if(!this.state.finishTime && !this.state.date) return
        /*const newItems=[
            ... this.state.items,
            {
                key: Date.now(),
                date: this.state.date,
                day:this.state.day,
                finishTime:this.state.finishTime,
                city:this.state.city
  
            }
        ]*/
        this.sendData()
        this.getData()
        //this.handleState(newItems,newItems)
  
    }

    handleRemoveItem(key){
        const newItem=this.state.items.filter((item)=>{
            return item._id !== key
        })
        this.handleState(newItem,newItem)
        this.deleteData(key)
        this.getData()
    }

    sumFunc(){
        this.handleAddItems()
        //this.sendData()       
    }
  
  handleClearData(){
    if(!this.state.finishTime && !this.state.date) return
    const newItems=[
        ... this.state.items,
        {
            key: Date.now(),
            date: this.state.date,
            day:this.state.day,
            finishTime:this.state.finishTime,
            city:this.state.city
        }
    ]
    this.handleState(newItems,newItems,{finishTime:'', date: ''})

  }

  _toggleModal = () =>
    this.setState({ isModalVisible: !this.state.isModalVisible });
   
    Back=()=>{
        this.props.navigation.goBack()    
    }

  render() {
    return (
      
      <View style={styles.column}> 
        <ImageBackground style={ styles.imgBackground } 
                 resizeMode='cover' 
                 source={require('../picture/fondo2.png')}>
            

            
            <Modal 
                style={styles.modalTop}
                backdropOpacity={0.9} 
                animationIn={'slideInLeft'}
                animationOut={'slideOutRight'}
                isVisible={this.state.isModalVisible}
                backdropColor='#696A69'
                transparent = {true}
            >
            <InputAgendar
                onChangeDate={this.onChangeDate}
                onChangeFinishTime={this.onChangeFinishTime}
                handleAddItems={this.handleAddItems}
                date={this.state.date}
                finishTime={this.state.finishTime}
                setDay={this.setDay}
                day={this.state.day}
                days={days}
                citys={citys}
                sendData={this.sendData}
                sumFunc={this.sumFunc}
                onChangeCity={this.onChangeCity}
                city={this.state.city}
                title={this.state.title}
            />

                <TouchableOpacity onPress={this._toggleModal}
                style={styles.button}
                >
                    <Text 
                    style={{fontSize:22,color:'black',textAlign:'center'}}
                    >
                        Volver
                    </Text>
                </TouchableOpacity>
            </Modal>

            <ItemsAgendar2
            onChangeLu={this.onChangeLu}
            onChangeMa={this.onChangeMa}
            onChangeMi={this.onChangeMi}
            onChangeJu={this.onChangeJu}
            onChangeVi={this.onChangeVi}
            onChangeSa={this.onChangeSa}
            onChangeDo={this.onChangeDo}
            lunes={this.state.lunes}
            martes={this.state.martes}
            miercoles={this.state.miercoles}
            jueves={this.state.jueves}
            viernes={this.state.viernes}
            sabado={this.state.sabado}
            domingo={this.state.domingo}

                onChangeSwitch={this.onChangeSwitch}
                isChecked={this.state.isChecked}
                items={this.state.items}
                dataSource={this.state.dataSource}
                handleRemoveItem={this.handleRemoveItem}
            />

                <TouchableOpacity
                    style={styles.button}
                    onPress={this._toggleModal}>
                        <Text
                        style={{fontSize:22,textAlign:'center',color:'black'}}
                        >
                            Agregar horario
                        </Text>
                </TouchableOpacity>
                    
        </ImageBackground>        
      </View>
      
    );
  }
}
/*
<TouchableOpacity
                    style={styles.button}
                    onPress={this.Back}
                >
                    <Text style={styles.text}>Volver</Text>
                </TouchableOpacity>
*/

const styles = StyleSheet.create({
  column:{
    flex: 1,
    flexDirection:'column',
    //marginVertical:150,
    //backgroundColor:'grey'
    backgroundColor:'transparent'
  },
  /*row: {
    flexDirection:'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    //backgroundColor: 'red',
    backgroundColor:'transparent',
    paddingVertical:10,
    backgroundColor:'#ffffff',
    marginBottom:5,
  },*/
  button:{
    backgroundColor:'#8CD2CC',
    width:220,
    margin:8,
    alignSelf:'center',
    //marginTop:100,
    //marginLeft:80,
    //marginRight:80,
    borderWidth: 2,
    borderRadius:10

  },
  modalTop:{
    flex:1,
  }, 
  text:{
    alignSelf:'center',
    fontSize:20,
    textAlign:'center',
    //fontWeight:'bold',
    color:'black'
  },
  imgBackground: {
    width: '100%',
    height: '100%',
    //justifyContent: 'center',
    //alignItems: 'center',
    //flex: 1 
},
});
