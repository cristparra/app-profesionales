import React, { Component } from 'react';
import {
  Text,
  View,
  StyleSheet,
  TouchableOpacity,
  Button,
  ImageBackground,
  Alert
} from 'react-native';
import { Agenda} from 'react-native-calendars';
import {LocaleConfig} from 'react-native-calendars';

LocaleConfig.locales['ch'] = {
  monthNames: ['Enero','Febrero','Marso','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'],
  monthNamesShort: ['En.','Feb.','Mzo','Abr.','My.','Jun.','Jul.','Ag.','Sept.','Oct.','Nov.','Dic.'],
  dayNames: ['Domingo','Lunes','Martes','Miercoles','Jueves','Viernes','Sabado'],
  dayNamesShort: ['Dom.','Lun.','Mart.','Miérc.','Juev.','Vier.','Sáb.']
};

LocaleConfig.defaultLocale = 'ch';

export default class App extends Component {
  constructor(props) {
    super(props);
    hoy = new Date().toJSON().slice(0,10)
   //alert(Date())
    this.state = {
      items: {
          '2018-11-30': [{name: 'Atención a domicilio',
                          description:'Dirección: Javiera Carrera 252, Valparaíso',
                          rangohorario:'Desde: 10:00',
                          nombrePaciente:'Nombre: Cesia Suazo'
                        }],
          '2018-10-27': [{name: 'evento 2 '}],
          '2018-10-28': [{name:"evento 3",description:"soy el description",rangohorario:'14:00 - 16:00'},{name:"evento 3.1",description:"soy el description",rangohorario:'17:00 - 18:00'}],
          '2018-10-29': [{name: 'evento 4'},{name: '4.1'}],
      },
      selected:{
          hoy
      }
    };
  } 

  render() {
    return (
      <View style={{height: 650}}>
        
          <Agenda
          

            items={this.state.items}
            loadItemsForMonth={this.loadItems.bind(this)}
            //selected={this.state.selected}
            renderItem={this.renderItem.bind(this)}
            renderEmptyDate={this.renderEmptyDate.bind(this)}
            rowHasChanged={this.rowHasChanged.bind(this)}
            // markingType={'period'}
            // markedDates={{
            //    '2017-05-08': {textColor: '#666'},
            //    '2017-05-09': {textColor: '#666'},
            //    '2017-05-14': {startingDay: true, endingDay: true, color: 'blue'},
            //    '2017-05-21': {startingDay: true, color: 'blue'},
            //    '2017-05-22': {endingDay: true, color: 'gray'},
            //    '2017-05-24': {startingDay: true, color: 'gray'},
            //    '2017-05-25': {color: 'gray'},
            //    '2017-05-26': {endingDay: true, color: 'gray'}}}
            // monthFormat={'yyyy'}
            // theme={{calendarBackground: 'red', agendaKnobColor: 'green'}}
            //renderDay={(day, item) => (<Text>{day ? day.day: 'item'}</Text>)}
          />
          
            <Button title="Volver" onPress={this.Back} />
            
          </View>
        );
      }
      Back =()=>{
        this.props.navigation.goBack()    
      }
      loadItems(day) {
        setTimeout(() => {
          

          for (let i = -15; i < 85; i++) {
            const time = day.timestamp + i * 24 * 60 * 60 * 1000;
            const strTime = this.timeToString(time);
            if (!this.state.items[strTime]) {
              this.state.items[strTime] = [];
              const numItems = Math.floor(Math.random() * 5);
              
            }
          }
          
          //console.log(this.state.items);
          const newItems = {};
          Object.keys(this.state.items).forEach(key => {newItems[key] = this.state.items[key];});
          this.setState({
            items: newItems
          });
        }, 1000);
        // console.log(`Load Items for ${day.year}-${day.month}`);
      }
      

      moreInfo(){
        Alert.alert("Atención a domicilio",
        "Nombre: Cesia Suazo\n Dirección: Javiera Carrera 252, Valparaíso\n Fecha: 30-11-2018 \n Hora: 10:00 \n Tipo de atención: Terapia \n Número telefónico: +569 81639257")
      }

      renderItem(item) {
        
        return (
          <View style={[styles.item, {height: item.height}]}>
            <Text style={{fontSize:20}}>{item.name}</Text>
            <Text style={{fontSize:15}}>{item.nombrePaciente}</Text>
            <Text style={{fontSize:15}}>{item.description}</Text>
            <Text>{item.rangohorario}</Text>
            <TouchableOpacity onPress={()=>this.moreInfo()}>
              <Text style={{color:'#8CD2CC'}}>Más información</Text>
            </TouchableOpacity>
          </View>
        );
      }

      agregarDisponibilidad=()=>{
        this.state.items['2018-10-08'].push({
           name: 'evento 9'
        });
        //alert(this.state.items)
       
      }
      renderEmptyDate() {
        return (
          <View style={styles.emptyDate}>
            
            <Text>Sin eventos!</Text>
          </View>
        );
      }

      rowHasChanged(r1, r2) {
        return r1.name !== r2.name;
      }

      timeToString(time) {
        const date = new Date(time);
        return date.toISOString().split('T')[0];
      }
  
}

const styles = StyleSheet.create({
  item: {
    backgroundColor: 'white',
    flex: 1,
    borderRadius: 5,
    padding: 10,
    marginRight: 10,
    marginTop: 17
  },
  emptyDate: {
    height: 15,
    flex:1,
    paddingTop: 30
  },imgBackground: {
    width: '100%',
    height: '100%'
  }
});