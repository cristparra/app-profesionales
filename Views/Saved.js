import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  ImageBackground,
  Button,
  AsyncStorage
} from 'react-native';


export default class Explore extends Component {
  render() {
    return (
        <View style={styles.container}>
          <ImageBackground style={ styles.imgBackground } 
                 resizeMode='cover' 
                 source={require('../picture/fondo2.png')}>

          </ImageBackground>
        </View>
    );
  }

  _signOutAsync = async () => {
    await AsyncStorage.clear();
    this.props.navigation.navigate('Auth');
  };
  
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  imgBackground: {
    width: '100%',
    height: '100%',
    justifyContent: 'center',
    alignItems: 'center',
    flex: 1 
},
});
