import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  ImageBackground,
  TextInput,
  TouchableOpacity,
  ScrollView,
  TouchableHighlight,
  Image,
  Picker
} from 'react-native';
import * as global from '../global/const'
import DatePicker from 'react-native-datepicker'


import moment from 'moment'
import t from 'tcomb-form-native'

//const t = require('tcomb-form-native');

const Form = t.form.Form


let carreras = [
    {value: '5bc023f143caaf0015b62f7d', text: 'Enfermera'},
    {value: '5bc08c6110b2cf001519ab1a', text: 'Kinesiólogo'},
    {value: '5bcf7e761c2e8f0015bab01f', text: 'Nutricionista'},
    {value: 'Podólogo', text: 'Podólogo'},
    {value: '5bcf7ecd1c2e8f0015bab021', text: 'Terapeuta Ocupacional'},
    {value: '5bcf7ef71c2e8f0015bab029', text: 'Técnicos en Salud'},

  ];

let universidades = [
    {value:'Pontificia Universidad Católica de Chile',text:'Pontificia Universidad Católica de Chile'},
    {value:'Universidad Andrés Bello',text:'Universidad Andrés Bello'},
    {value:'Universidad Austral De Chile',text:'Universidad Austral De Chile'},
    {value:'Universidad Católica Del Maule',text:'Universidad Católica Del Maule'},
    {value:'Universidad Católica Del Norte',text:'Universidad Católica Del Norte'},
    {value:'Universidad De Antofagasta',text:'Universidad De Antofagasta'},
    {value:'Universidad De Chile',text:'Universidad De Chile'},
    {value:'Universidad De Concepción',text:'Universidad De Concepción'},
    {value:'Universidad Del Desarrollo',text:'Universidad Del Desarrollo'},
    {value:'Universidad De La Frontera',text:'Universidad De La Frontera'},
    {value:'Universidad De Los Andes',text:'Universidad De Los Andes'},
    {value:'Universidad De Santiago De Chile',text:'Universidad De Santiago De Chile'},
    {value:'Universidad De Talca',text:'Universidad De Talca'},
    {value:'Universidad De Valparaíso',text:'Universidad De Valparaíso'},
    {value:'Universidad Diego Portales',text:'Universidad Diego Portales'},
    {value:'Universidad Mayor',text:'Universidad Mayor'},
    {value:'Universidad San Sebastián',text:'Universidad San Sebastián'},
]
 

const UserRegister = t.struct({
    name:t.String,
    lastname:t.String,
    email: t.String,
    rut:t.String,
    phone:t.String,
    address: t.String,
    dateOfBirth: t.Date,
    //profesions:t.enums.of('Audi Chrysler Ford Renault Peugeot'),
    password:  t.String,
    
})

const options = {
  fields: {
    name:{
        label:"Nombre"
    },
    lastname:{
        label:"Apellidos"
    },
    email: {
        label:"Correo",
        autoCapitalize: 'none',
        autoCorrect: false
    },
    rut:{
        label:"Rut",
        placeholder:"12345678-9"
    },
    phone:{
        label:"Teléfono",
        placeholder:"569-123-45678"
    },
    address:{
        label:"Dirección"
    },
    
    password: {
        label:"Contraseña",
        autoCapitalize: 'none',
        password: true,
        autoCorrect: false,
        secureTextEntry: true,
    },
    dateOfBirth:{
        label:'Fecha de nacimiento',
        mode:'date',
        config: {
            format: (date) => moment(date).format('YYYY-MM-DD'),
          },
        
    },
   /* profesions:{
        
            options: [
              {value: 'Audi', text: 'Audi'}, // an option
               // a group of options
                {value: 'Chrysler', text: 'Chrysler'},
                {value: 'Ford', text: 'Ford'}
              ,
               // another group of options
                {value: 'Renault', text: 'Renault'},
                {value: 'Peugeot', text: 'Peugeot'}
              , // use `disabled: true` to disable an optgroup
            ]
          
    }*/
    

  },
  //stylesheet: formStyles,
  
}

var prof=[];
var profes={}

export default class Register extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            value: {
                name:"",
                lastname:"",
                email: '',
                rut:'',
                phone:"",
                address:"",
                password: '',
            },
            profession:'',
            universidad:'',
            seniorYear:''
        };
      }


      onChangeSeniorYear(seniorYear){
          this.setState({
              seniorYear:seniorYear
          })
      }
    _onChange = (value) => {
        this.setState({
        value
        })
    }

    _handleAdd = () => {
        const value = this.refs.form.getValue();
        // If the form is valid...
        if (value) {
        /*const data = {
            name:value.name,
            lastname:value.lastname,
            email: value.email,
            rut:value.rut,
            phone:value.phone,
            address:value.address,
            dateOfBirth:value.dateOfBirth,
            password: value.password,
            //idProfession:value.profesion
        }*/
        // Serialize and post the data
       // const json = JSON.stringify(data);
        fetch(global.API_REGISTER, {
            method: 'POST',
            headers: {
            'Content-Type': 'application/json',
            Accept: 'application/json'
            },
            body: JSON.stringify({
                "name": value.name,
                "rut": value.rut,
                "genre":1,
                "lastName": value.lastname,
                "email": value.email,
                "phone": value.phone,
                "address": value.address,
                "password": value.password,
                "dateOfBirth": value.dateOfBirth,
                "personalPhoto": {
                    "url": "5b8d9eca280656228c482825"
                },
                "professionalTitle": {
                    "idProfession": this.state.profession,
                    "university": this.state.universidad,
                    "seniorYear": this.state.seniorYear
                },
                "attentionsAvailable": [
                    {
                        "typeId": "5bc023ce43caaf0015b62f7c"
                    },
                    {
                        "typeId":"5bcf7e3b1c2e8f0015bab01a"
                    },
                    {
                        "typeId":"5bc08d2a3831000015ef8921"
                    },
                    {
                        "typeId":"5bc09b4f8972020015ab2b64"
                    }
                ]
            })
        })
        .then((response) => response.text())
        .then((responseText) => {
            console.log("respuesta send data:",responseText)
            console.log("id profesion: ",this.state.profession)
            if (responseText.error) {
                console.log("error:",responseText);
            } else {
                
                alert(responseText +"      ")

            }
        })
        .catch((error) => {
            console.log(error);
            this.onLoginFail();
            alert('There was an error logging in.');
        })
        .done()
        } else {
        // Form validation error
        alert('Please fix the errors listed and try again.')
        }
        this.Back();
    }

    componentWillMount(){
        this.getProfession()
    }

    onChangeProfession(profession){
        this.setState({profession:profession})
    }

    onChangeUniversidades(universidad){
        this.setState({universidad:universidad})
    }
    getProfession (){
        
                fetch(global.API_GET_PROFESSION, {
                  method:'GET',
                    headers: {
                        'Content-Type': 'application/json',
                        Accept: 'application/json'
                    },

                })
                .then((response) => response.json())
                .then((responseJson) => {
                    console.log("responseJson: ",responseJson)
                   /* this.setState({
                        profession:responseJson
                    })*/
                    profes=responseJson

                    //lista de profesionales
                  /*  for(let i  = 0; i<responseJson.length; i++){
                   
                        prof.push(responseJson[i].name)
                        console.log(responseJson[i].name);                     
                    
                    }*/
                    
                  
                    //console.log("professions: ",prof)

                })
                .catch((error) => {
                    console.log(error);
                });

    }

    Back=()=>{
        this.props.navigation.goBack()    
    }
    render() {
        
      return (
        <ScrollView style={styles.container}>

                <View style={styles.container2}>

                    <ImageBackground style={ styles.imgBackground } 
                    resizeMode='cover' 
                    source={require('../picture/fondo2.png')}>
                        <Form 
                        ref='form'
                        options={options}
                        type={UserRegister}
                        value={this.state.value}
                        onChange={this._onChange}
                        
                        />

                        <Picker
                                //prompt='Seleccionar carrera'
                                mode='dialog'
                                selectedValue={this.state.universidad}
                                onValueChange={itemValue => this.onChangeUniversidades(itemValue)}>
                                <Picker.Item key={'unselectable'} label={"Selecciona tu universidad"} value={null} />
                                {universidades.map((i, index) => (
                                
                                <Picker.Item key={index} color={'black'} label={i.text} value={i.value} 
                                
                                />
                                
                                ))}
                        </Picker>

                        <Picker
                                //prompt='Seleccionar carrera'
                                mode='dialog'
                                selectedValue={this.state.profession}
                                onValueChange={itemValue => this.onChangeProfession(itemValue)}>
                                <Picker.Item key={'unselectable'} label={"Selecciona tu carrera"} value={null} />
                                {carreras.map((i, index) => (
                                
                                <Picker.Item key={index} color={'black'} label={i.text} value={i.value} 
                                
                                />
                                
                                ))}
                        </Picker>

                         <DatePicker
                            date={this.state.seniorYear}
                            style={{width:390,padding:8}}
                            mode='date'
                            placeholder='Año de título'
                            format= 'YYYY-MM-DD'
                            //minDate='2018-10-07'
                            //maxDate='2018-12-13'
                            confirmBtnText='Confirm'
                            cancelBtnText='Cancel'
                            onDateChange={ (date) => this.onChangeSeniorYear(date) }
                            customStyles={{
                                placeholderText: {
                                    color:'black'
                                },
                                dateTouchBody: {borderColor:"black",backgroundColor:'white'}
                            }}
                        />

                        <TouchableHighlight onPress={this._handleAdd}>
                        <Text style={[styles.button, styles.greenButton]}>Enviar </Text>
                        </TouchableHighlight>

                        <TouchableHighlight onPress={this.Back}>
                        <Text style={styles.transparentButton}>Volver</Text>
                        </TouchableHighlight>
                    </ImageBackground>

                </View>
        </ScrollView>
      );
    }
  }



  const styles = StyleSheet.create({
    container: {
      //padding: 20,
      flex: 1,
      flexDirection: 'column',
      backgroundColor: '#ffffff'
    },
    container2: {
        padding: 20,
        flex: 1,
        flexDirection: 'column',
        backgroundColor: '#ffffff'
      },
    imgBackground: {
      width: '100%',
      height: '100%',
  },
  logo:{
      flex:.1,
      justifyContent: 'center',
      alignItems: 'center',
      //paddingLeft:50,
      //paddingRight:50,
      width:220,
      height:220,
    },
    button: {
      borderRadius: 4,
      padding: 20,
      textAlign: 'center',
      marginBottom: 20,
      //fontSize: 20,
      color: 'black'
    },
    greenButton: {
      backgroundColor: '#446E6E'
    },
    transparentButton:{
      padding:8,
      marginBottom:20,
      color:'black',
      backgroundColor:'#446E6E',
      textAlign:'center',
      alignSelf:'center',
      width:180,
      //fontSize: 20,
      },
    centering: {
      alignItems: 'center',
      justifyContent: 'center'
    },
    
  })
  