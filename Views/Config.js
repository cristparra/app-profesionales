import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  ImageBackground,
  Button,
  AsyncStorage,
  TouchableOpacity,
} from 'react-native';


export default class Config extends Component {
  modificarUser=()=>{
    this.props.navigation.navigate('User');
  }
  render() {
    return (
        <View style={styles.container}>
            <ImageBackground style={ styles.imgBackground } 
                 resizeMode='cover' 
                 source={require('../picture/fondo2.png')}>

              <TouchableOpacity
                style={[styles.button,{margin:10}]}
                onPress={this.modificarUser}>
                <Text
                  style={{fontSize:20,textAlign:'center',color:'black'}}
                >
                  Modificar datos personales
                </Text>
              </TouchableOpacity>
              
              <TouchableOpacity
                style={styles.button}
                onPress={this._signOutAsync}>
                <Text
                  style={{fontSize:20,textAlign:'center',color:'black'}}
                >
                  Cerrar sesión
                </Text>
              </TouchableOpacity>
          
            </ImageBackground>
        </View>
    );
  }

  _signOutAsync = async () => {
    await AsyncStorage.removeItem('id_token');
    this.props.navigation.navigate('Auth');
  };
  
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  imgBackground: {
    width: '100%',
    height: '100%',
    justifyContent: 'center',
    alignItems: 'center',
    //flex: 1 
  },
  button:{
    backgroundColor:'#8CD2CC',
    width:220,
    margin:8,
    alignSelf:'center',
    //marginTop:100,
    //marginLeft:80,
    //marginRight:80,
    borderWidth: 2,
    borderRadius:10

  },
});
