import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  ImageBackground,
  TouchableHighlight,
  TouchableOpacity,
  ScrollView,
  AsyncStorage,
  Button
} from 'react-native';
import * as global from '../global/const'
import t from 'tcomb-form-native'

const Form = t.form.Form

const UserRegister = t.struct({
    name:t.String,
    lastname:t.String,
    email: t.String,
    rut:t.String,
    phone:t.String,
    address: t.String,
    //dateOfBirth: t.Date,
    password:  t.String,
    
})

const options = {
  fields: {
    name:{
        label:"Nombre",
        fontWeight:'bold',
        color:'blue'
    },
    lastname:{
        label:"Apellidos",
    },
    email: {
        label:"Correo",
        autoCapitalize: 'none',
        autoCorrect: false,
        
    },
    rut:{
        label:"Rut"
    },
    phone:{
        label:"Teléfono"
    },
    address:{
        label:"Dirección"
    },
    
    password: {
        label:"Contraseña",
        autoCapitalize: 'none',
        password: true,
        autoCorrect: false,
        secureTextEntry: true,
    },
   /* dateOfBirth:{
        label:'Fecha de nacimiento',
        mode:'date',
        config: {
            format: (date) => moment(date).format('YYYY-MM-DD'),
          },
    },
*/
  },
  //stylesheet: formStyles,
  
}

export default class ModifUser extends Component {
  constructor(props) {
    super(props);
    this.state = {
        data:'',
        value: {
          name:"",
          lastname:"",
          email: '',
          rut:'',
          phone:"",
          address:"",
          password: '',
        }
    };
  }

  //revisar token
 
  _onChange = (value) => {
    this.setState({
    value
    })
  }

  _getToken = async () => {
    try{
        const token = await AsyncStorage.getItem('id_token');
        //alert(token)
        return token;
        //this.props.navigation.navigate('App');
    }
    catch (error) {
        console.log('AsyncStorage Error: ' + error.message);
    }
};

Back=()=>{
  this.props.navigation.goBack()
}
  
  componentDidMount () {
    
    const tok=this._getToken().then((value) => {
      if (value !== null) {
          console.log("token: ", value);
          
          //alert(value)
          // Serialize and post the data
          fetch(global.API_GET_DATA, {
            method:'GET',
              headers: {
                'x-auth-token': value,
                },
          })
          .then((response) => response.json())
          .then((responseJson) => {
              console.log(responseJson)
            
              //alert(responseText)
              
              this.setState({
               value:{
                 name:responseJson.name,
                 lastname:responseJson.lastName,
                 email:responseJson.email,
                 phone:responseJson.phone,
                 address:responseJson.address,
                 rut:responseJson.rut,
               }
              })
          })
          .catch((error) => {
              console.log(error);
              //alert('error en el get.');
          })
          

    }
  });

  
}



  render() {
    return (
      <ScrollView style={{flex:1,}}>
        <View style={styles.container}>
        <ImageBackground style={ styles.imgBackground } 
          resizeMode='cover' 
          source={require('../picture/fondo2.png')}>  
              <Text style={styles.welcome}>
              Modificar datos 
              </Text>

              <Form 
                ref='form'
                options={options}
                type={UserRegister}
                value={this.state.value}
                onChange={this._onChange}
              />
              
              <TouchableHighlight onPress={this.onPress}>
                <Text style={styles.button}>Modificar</Text>
              </TouchableHighlight>

              <Button title="Volver" onPress={this.Back} />

          </ImageBackground>  
        </View>
      </ScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
    color:'black',
    fontWeight:'bold',

  },
  imgBackground: {
    width: '100%',
    height: '100%',
},
});
