import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  ImageBackground,
  TextInput,
  TouchableOpacity,
  ScrollView,
  TouchableHighlight,
  Image,
  Picker
} from 'react-native';
import * as global from '../global/const'

import moment from 'moment'




let carreras = [
    {value: '5bc023f143caaf0015b62f7d', label: 'Enfermera'},
    {value: '5bc08c6110b2cf001519ab1a', label: 'Kinesiologo'},
    {value: '5bcf7e761c2e8f0015bab01f', label: 'Nutricionista'},
    {value: 'Podólogo', label: 'Podólogo'},
    {value: '5bcf7ecd1c2e8f0015bab021', label: 'Terapeuta Ocupacional'},
    {value: '5bcf7ef71c2e8f0015bab029', label: 'Técnicos en Salud'},

  ];

export default class Register extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            
                nombre:"cristian",
                apellidos:"parra",
                correo: 'cristian@gmail.com',
                rut:'17673496-5',
                telefono:"569-751-83778",
                direccion:"Mercedes 461",
                contraseña: '1234',
                fechaDeNacimiento:'',
                profession:''
            
        };
      }



    _handleAdd = () => {
    
        fetch(global.API_REGISTER, {
            method: 'POST',
            headers: {
            'Content-Type': 'application/json',
            Accept: 'application/json'
            },
            body: JSON.stringify({
                "name": this.state.nombre,
                "rut": this.state.rut,
                "genre":1,
                "lastName": this.state.apellidos,
                "email": this.state.correo,
                "phone": this.state.telefono,
                "address": this.state.direccion,
                "password": this.state.contraseña,
                "dateOfBirth": this.state.fechaDeNacimiento,
                "personalPhoto": {
                    "url": "5b8d9eca280656228c482825"
                },
                "professionalTitle": {
                    "idProfession": "5bc023f143caaf0015b62f7d",
                    "university": "Universidad Del Mar",
                    "seniorYear": "2015-01-01"
                },
                "attentionsAvailable": [
                    {
                    "typeId": "5bc023ce43caaf0015b62f7c"
                    }
                ]
            })
        })
        .then((response) => response.text())
        .then((responseText) => {
            console.log(responseText)
            console.log("profession piker: ",this.state.profession)
            if (responseText.error) {
                console.log(responseText);
            } else {
                
                alert(responseText +"      ")

            }
        })
        .catch((error) => {
            console.log(error);
            this.onLoginFail();
            alert('There was an error logging in.');
        })
        .done()
        
    }

    componentWillMount(){
        this.getProfession()
    }


    getProfession (){
        
                fetch(global.API_GET_PROFESSION, {
                  method:'GET',
                    headers: {
                        'Content-Type': 'application/json',
                        Accept: 'application/json'
                    },

                })
                .then((response) => response.json())
                .then((responseJson) => {
                    console.log("responseJson: ",responseJson)
                    this.setState({
                        profession:responseJson
                    })
                  /*  for(let i  = 0; i<responseJson.length; i++){
                        this.setState({
                            profession:[...this.state.profession,responseJson[i].name]
                        })
                            console.log(responseJson[i].name);                     
                    
                    }
                    */
                  
                    console.log("professions: ",this.state.profession)

                })
                .catch((error) => {
                    console.log(error);
                });

    }

    Back=()=>{
        this.props.navigation.goBack()    
    }

    onChangeProfession(profession){
        this.setState({profession:profession})
    }


    render() {
        
      return (
        <ScrollView style={styles.container}>

                <View style={styles.container2}>

                    <ImageBackground style={ styles.imgBackground } 
                    resizeMode='cover' 
                    source={require('../picture/fondo2.png')}>
                        
                <TextInput
                    style={{height: 40, borderColor: 'grey', borderWidth: 1,margin:15,textAlign:'center'}}
                    onChangeText={(nombre) => this.setState({nombre})}
                    placeholder="Nombre"
                    value={this.state.nombre}
                    keyboardType={'default'}
                />
                <TextInput
                    style={{height: 40, borderColor: 'grey', borderWidth: 1,margin:15,textAlign:'center'}}
                    onChangeText={(apellidos) => this.setState({apellidos})}
                    placeholder="Apellidos"
                    value={this.state.apellidos}
                    keyboardType={'default'}
                />
                <TextInput
                    style={{height: 40, borderColor: 'grey', borderWidth: 1,margin:15,textAlign:'center'}}
                    onChangeText={(correo) => this.setState({correo})}
                    placeholder="Correo electrónico"
                    value={this.state.correo}
                    keyboardType={'email-address'}
                />
                <TextInput
                    style={{height: 40, borderColor: 'grey', borderWidth: 1,margin:15,textAlign:'center'}}
                    onChangeText={(rut) => this.setState({rut})}
                    placeholder="Rut"
                    value={this.state.rut}
                    keyboardType={'default'}
                />
                <TextInput
                    style={{height: 40, borderColor: 'grey', borderWidth: 1,margin:15,textAlign:'center'}}
                    onChangeText={(telefono) => this.setState({telefono})}
                    placeholder="Teléfono"
                    value={this.state.telefono}
                    keyboardType={'numeric'}
                />
                <TextInput
                    style={{height: 40, borderColor: 'grey', borderWidth: 1,margin:15,textAlign:'center'}}
                    onChangeText={(direccion) => this.setState({direccion})}
                    placeholder="Dirección"
                    value={this.state.direccion}
                    keyboardType={'default'}
                />
                <TextInput
                    style={{height: 40, borderColor: 'grey', borderWidth: 1,margin:15,textAlign:'center'}}
                    onChangeText={(contraseña) => this.setState({contraseña})}
                    placeholder="Contraseña"
                    value={this.state.contraseña}
                    keyboardType={'default'}
                    secureTextEntry={true}
                />

                <Picker
                
                        //style={{justifyContent:'center'}}
                        //backgroundColor
                        //CAmbiar el color del fondo se puede solo a través de android nativo https://stackoverflow.com/questions/38921492/how-to-style-the-standard-react-native-android-picker/39141949#39141949
                        prompt='Seleccionar carrera'
                        mode='dialog'
                        selectedValue={this.state.profession}
                        onValueChange={itemValue => this.onChangeProfession(itemValue)}>
                        {carreras.map((i, index) => (
                        <Picker.Item key={index} color={'black'} label={i.label} value={i.value} 
                        
                        />
                        ))}
                </Picker>



                        <TouchableHighlight onPress={this._handleAdd}>
                        <Text style={[styles.button, styles.greenButton]}>Entrar</Text>
                        </TouchableHighlight>

                        <TouchableHighlight onPress={this.Back}>
                        <Text style={styles.transparentButton}>Volver</Text>
                        </TouchableHighlight>
                    </ImageBackground>

                </View>
        </ScrollView>
      );
    }
  }



  const styles = StyleSheet.create({
    container: {
      //padding: 20,
      flex: 1,
      flexDirection: 'column',
      backgroundColor: '#ffffff'
    },
    container2: {
        padding: 20,
        flex: 1,
        flexDirection: 'column',
        backgroundColor: '#ffffff'
      },
    imgBackground: {
      width: '100%',
      height: '100%',
  },
  logo:{
      flex:.1,
      justifyContent: 'center',
      alignItems: 'center',
      //paddingLeft:50,
      //paddingRight:50,
      width:220,
      height:220,
    },
    button: {
      borderRadius: 4,
      padding: 20,
      textAlign: 'center',
      marginBottom: 20,
      //fontSize: 20,
      color: '#fff'
    },
    greenButton: {
      backgroundColor: '#67b280'
    },
    transparentButton:{
      padding:8,
      marginBottom:20,
      color:'black',
      backgroundColor:'#123456',
      textAlign:'center',
      alignSelf:'center',
      width:180,
      //fontSize: 20,
      },
    centering: {
      alignItems: 'center',
      justifyContent: 'center'
    },
    
  })
  