import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  TouchableHighlight,
  ScrollView,
  AsyncStorage,
  Image,
  ImageBackground,
  TextInput

} from 'react-native';

import * as global from '../global/const'


export default class LoginView extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            correo:'',
            contraseña:''

        };
    }


    _signInAsync = async (token) => {
        try{
            await AsyncStorage.setItem('id_token', token);
            this.props.navigation.navigate('App');
        }
        catch (error) {
            console.log('AsyncStorage Error: ' + error.message);
        }
    };

    login = () => {
        fetch(global.API_LOGIN, {
            method: 'POST',
            headers: {
            'Content-Type': 'application/json',
            Accept: 'application/json'
            },
            body: JSON.stringify({
            "email":this.state.correo,
            "password":this.state.contraseña
            })
        })
        .then((response) => response.text())
        .then((responseText) => {
            console.log(this.state.correo)
            console.log(this.state.contraseña)
            console.log(responseText)

            if (responseText.error) {
            //alert(res.error)
            } else if (responseText == "Invalid email or password") {
                alert(responseText)
            }
            else {
                this._signInAsync(responseText);    
            }
        })
        .catch((error) => {
            console.log(error);
            alert('There was an error logging in.');
        })
        .done()
        
    }

    goToRegister=()=>{
        this.props.navigation.navigate('Register');
    }
    


    render() {
        return (
    
        <ScrollView style={styles.container}>
            <ImageBackground style={ styles.imgBackground } 
                 resizeMode='cover' 
                 source={require('../picture/fondo2.png')}>
            
            <Image
                style={styles.logo}
                source={require('../picture/logo.png')}
                imageStyle={{ resizeMode: 'contain' }}
          />
                <Text style={{fontSize:22,textAlign:'center',color:'black'}}>Personal de la salud</Text>
            </ImageBackground>
            
            <View style={styles.container2}>
      
                <TextInput
                    style={{height: 40, borderColor: 'grey', borderWidth: 1,margin:15,textAlign:'center'}}
                    onChangeText={(correo) => this.setState({correo})}
                    placeholder="Correo electrónico"
                    value={this.state.correo}
                    keyboardType={'email-address'}
                />
                <TextInput
                    style={{height: 40, borderColor: 'grey', borderWidth: 1,margin:15,textAlign:'center' }}
                    onChangeText={(contraseña) => this.setState({contraseña})}
                    placeholder="Contraseña"
                    value={this.state.contraseña}
                    secureTextEntry={true}
                />

                <TouchableHighlight onPress={this.login}>
                <Text style={[styles.button, styles.greenButton]}>Entrar</Text>
                </TouchableHighlight>

                <TouchableHighlight onPress={this.goToRegister}>
                <Text style={[styles.transparentButton, {margin:10,backgroundColor:'#B0BAB0'}]}>Registrar</Text>
                </TouchableHighlight>

                <TouchableHighlight onPress={this.onChange}>
                <Text style={[styles.transparentButton,{margin:10,backgroundColor:'#B0BAB0'}]}>Olvidé mi contraseña</Text>
                </TouchableHighlight>
            </View>

        </ScrollView>
        )
    }
}

    
const styles = StyleSheet.create({
  container: {
    //padding: 20,
    flex: 1,
    flexDirection: 'column',
    backgroundColor: '#ffffff'
  },
  container2: {
    padding: 20,
    flex: 1,
    flexDirection: 'column',
    backgroundColor: '#ffffff'
  },
  imgBackground: {
    width: '100%',
    height: '100%',
    justifyContent: 'center',
    alignItems: 'center',
    flex: 1 
},
logo:{
    flex:.1,
    justifyContent: 'center',
    alignItems: 'center',
    //paddingLeft:50,
    //paddingRight:50,
    width:220,
    height:220,
  },
  button: {
    borderRadius: 4,
    padding: 20,
    textAlign: 'center',
    marginBottom: 20,
    //fontSize: 20,
    color: 'black',
  },
  greenButton: {
    backgroundColor: '#446E6E'
  },
  transparentButton:{
    padding:8,
    marginBottom:8,
    color:'black',
    backgroundColor:'#446E6E',
    textAlign:'center',
    alignSelf:'center',
    width:180,
    //fontSize: 20,
    },
  centering: {
    alignItems: 'center',
    justifyContent: 'center'
  },
  
})

