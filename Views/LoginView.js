import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  TouchableHighlight,
  ScrollView,
  AsyncStorage,
  Image,
  ImageBackground,

} from 'react-native';

import * as global from '../global/const'
import t from 'tcomb-form-native'

var _ = require('lodash');

// clone the default stylesheet
const stylesheet = _.cloneDeep(t.form.Form.stylesheet);

// overriding the text color
stylesheet.textbox.normal.color = 'black';
stylesheet.textbox.normal.textAlign='center'
stylesheet.controlLabel.normal.textAlign='center'
//stylesheet.textbox.normal.lineHeight='0'


const Form = t.form.Form

const User = t.struct({
  correo: t.String,
  contraseña:  t.String
})

const options = {
  fields: {
    correo: {
      autoCapitalize: 'none',
      autoCorrect: false,
      error:'Enter valid email',
      stylesheet:stylesheet

    },
    contraseña: {
      autoCapitalize: 'none',
      password: true,
      secureTextEntry: true,
      autoCorrect: false,
      stylesheet:stylesheet
    }
  },
  //stylesheet: formStyles,
  
}



export default class LoginView extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            value: {
            correo: '',
            contraseña: ''
            }
        };
    }

    componentWillUnmount() {
        this.setState = {
        value: {
            correo: '',
            contraseña: null
        }
        }
    }

    _onChange = (value) => {
        this.setState({
        value
        })
    }
    _signInAsync = async (token) => {
        try{
            await AsyncStorage.setItem('id_token', token);
            this.props.navigation.navigate('App');
        }
        catch (error) {
            console.log('AsyncStorage Error: ' + error.message);
        }
    };

    login = () => {
        const value = this.refs.form.getValue();
        // If the form is valid...
        if (value) {
        const data = {
            correo: value.correo,
            contraseña: value.contraseña
        }
        // Serialize and post the data
        const json = JSON.stringify(data);
        fetch(global.API_LOGIN, {
            method: 'POST',
            headers: {
            'Content-Type': 'application/json',
            Accept: 'application/json'
            },
            body: JSON.stringify({
            "email":value.correo,
            "password":value.contraseña
            })
        })
        .then((response) => response.text())
        .then((responseText) => {
            console.log(responseText)
            if (responseText.error) {
                
            //alert(res.error)
            } else if (responseText == "Invalid email or password") {
                alert(responseText)
            }
            else {
                //alert(responseText +"      ")
                this._signInAsync(responseText);    
            
            
                //this.props.newJWT(responseText);
                //this.props.deleteJWT("id_token");
                //this.props.authSwitch();
            }
        })
        .catch((error) => {
            console.log(error);
            alert('There was an error logging in.');
        })
        .done()
        } else {
        // Form validation error
        alert('Please fix the errors listed and try again.')
        }
    }

    goToRegister=()=>{
        this.props.navigation.navigate('Register');
    }
    


    render() {
        return (
    
        <ScrollView style={styles.container}>
            <ImageBackground style={ styles.imgBackground } 
                 resizeMode='cover' 
                 source={require('../picture/fondo2.png')}>
            
            <Image
                style={styles.logo}
                source={require('../picture/logo.png')}
                imageStyle={{ resizeMode: 'contain' }}
          />
                <Text style={{fontSize:22,textAlign:'center',color:'black'}}>Personal de la salud</Text>
            </ImageBackground>
            
            <View style={styles.container2}>
                <Form style={styles.container2}
                ref='form'
                options={options}
                type={User}
                value={this.state.value}
                onChange={this._onChange}
                />
                

                <TouchableHighlight onPress={this.login}>
                <Text style={[styles.button, styles.greenButton]}>Entrar</Text>
                </TouchableHighlight>

                <TouchableHighlight onPress={this.goToRegister}>
                <Text style={styles.transparentButton}>Registrar</Text>
                </TouchableHighlight>

                <TouchableHighlight onPress={this.onChange}>
                <Text style={styles.transparentButton}>Olvidé mi contraseña</Text>
                </TouchableHighlight>
            </View>

        </ScrollView>
        )
    }
}


/*    const formStyles = {
        ...Form.stylesheet,
        formulario:{
            justifyContent: 'center',
            alignItems: 'center',
          },
    }
*/    
const styles = StyleSheet.create({
  container: {
    //padding: 20,
    flex: 1,
    flexDirection: 'column',
    backgroundColor: '#ffffff'
  },
  container2: {
    padding: 20,
    flex: 1,
    flexDirection: 'column',
    backgroundColor: '#ffffff'
  },
  imgBackground: {
    width: '100%',
    height: '100%',
    justifyContent: 'center',
    alignItems: 'center',
    flex: 1 
},
logo:{
    flex:.1,
    justifyContent: 'center',
    alignItems: 'center',
    //paddingLeft:50,
    //paddingRight:50,
    width:220,
    height:220,
  },
  button: {
    borderRadius: 4,
    padding: 20,
    textAlign: 'center',
    marginBottom: 20,
    //fontSize: 20,
    color: '#fff'
  },
  greenButton: {
    backgroundColor: '#67b280'
  },
  transparentButton:{
    padding:8,
    marginBottom:8,
    color:'black',
    backgroundColor:'#6166a8',
    textAlign:'center',
    alignSelf:'center',
    width:180,
    //fontSize: 20,
    },
  centering: {
    alignItems: 'center',
    justifyContent: 'center'
  },
  
})

