import React from 'react';
import {
  ActivityIndicator,
  AsyncStorage,
  Button,
  StatusBar,
  StyleSheet,
  View,
} from 'react-native';
import { createStackNavigator, createSwitchNavigator } from 'react-navigation';
import LoginView2 from './Views/LoginView2'
import LoginView from './Views/LoginView'
import Register from './Views/Register'
import Register2 from './Views/Register2'
import LoggedIn from './LoggedIn'
import Saved from './Views/Saved'
import Agenda from './Views/Agenda'
import Agendar from './Views/Agendar'
import ModifUser from './Views/ModifUser'


class AuthLoadingScreen extends React.Component {
  constructor() {
    super();
    this._bootstrapAsync();
  }

  // Fetch the token from storage then navigate to our appropriate place
  _bootstrapAsync = async () => {
    const userToken = await AsyncStorage.getItem('id_token');

    // This will switch to the App screen or Auth screen and this loading
    // screen will be unmounted and thrown away.
    this.props.navigation.navigate(userToken ? 'App' : 'Auth');
  };

  // Render any loading content that you like here
  render() {
    return (
      <View style={styles.container}>
        <ActivityIndicator />
        <StatusBar barStyle="default" />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
});

const AppStack = createStackNavigator(
  { 
    Home: LoggedIn, 
    Agenda: Agenda, 
    Agendar:Agendar, 
    User:ModifUser,
  },
  {
    headerMode: 'none'
  }
);
const AuthStack = createStackNavigator(
  { 
    LoginView: LoginView2,
    Register:Register,
  },
  {
    initialRouteName:'LoginView',
    headerMode: 'none',
  } 
);

export default createSwitchNavigator(
  {
    AuthLoading: AuthLoadingScreen,
    App: AppStack,
    Auth: AuthStack,
  },
  {
    initialRouteName: 'AuthLoading',
  }
);