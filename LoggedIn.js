import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  Image,
  Button,
  TouchableOpacity,
  AsyncStorage
} from 'react-native';

import{createBottomTabNavigator}from 'react-navigation'
import Saved from './Views/Saved'
import Config from './Views/Config'
import Agendar from './Views/Agendar'
import Agenda from './Views/Agenda'


export default createBottomTabNavigator({
    
    Agendar:{
      //screen:Explore
      screen:Agendar,
      navigationOptions:{
        tabBarLabel: '',
        title:"",
        tabBarIcon: ({tintColor }) =>( 
         <Image source={require('./picture/schedule.png')} style={{height:28,width:28,tintColor:tintColor}} />
        )
      }
      
    },
    Saved:{
        screen:Agenda,
        navigationOptions:{
          tabBarLabel: '',
          title:"",
          tabBarIcon: ({tintColor}) =>(
            <Image source={require('./picture/event.png')} style={{height:28,width:28,tintColor:tintColor}} />
          )
        }
      }
      ,
    Explore:{
        screen:Saved,
        navigationOptions:{
          tabBarLabel: '',
          title:"",
          tabBarIcon: ({tintColor}) =>(
            <Image source={require('./picture/user.png')} style={{height:28,width:28,tintColor:tintColor}} />
          )
        }
      },
      Config:{
          screen:Config,
          navigationOptions:{
            tabBarLabel: '',
            title:"",
            tabBarIcon: ({tintColor}) =>(
              <Image source={require('./picture/config.png')} style={{height:28,width:28,tintColor:tintColor}} />
            )
          }
        }

  },
  {
    tabBarOptions:{
        activeTintColor:'#446E6E',
        inactiveTintColor:'white',
        style:{
            backgroundColor:'#8CD2CC',
            borderTopWidth:0,
            shadowOffset:{width:5,height:3},
            shadowColor:'black',
            shadowOpacity:0.5,
            elevation:5
        }
    }
  }
  )