
export const  API = 'https://bhealthapp.herokuapp.com'
export const API_LOGIN = API + '/api/auth/professional'
export const API_REGISTER = API + '/api/registerProfessional'
export const API_GET_DATA = API + '/api/professionals/me'
export const API_SEND_SCHEDULE = API + '/api/schedules/me'
export const API_GET_SHHEDULE = API + '/api/schedules/me'
export const API_DELETE_SCHEDULE = API + '/api/schedules/me/'

export const API_GET_PROFESSION = API + '/api/professions'

