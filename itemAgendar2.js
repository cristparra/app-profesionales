import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  TextInput,
  TouchableOpacity,
  Switch,
  ScrollView,
  FlatList,
  Image,
  CheckBox
} from 'react-native';

export default class ItemAgendar2 extends Component {
    
    hourToString(hour){
        var newHour;
        if(hour<100){
            newHour = '00:'+ hour.toString();
        }
        else if(hour<1000){
            newHour = '0'+hour.toString().substr(0,1)+':'+hour.toString().substr(1,3);
        }
        else if(hour<=2400){
            newHour = hour.toString().substr(0,2)+':'+hour.toString().substr(2,4);
        }
        else return 'Error! Wrong format for hour.';
        return newHour;
    }
    render() {
      return (
        
        <View style={[styles.column,{backgroundColor:'transparent',marginTop:25}]}> 
            <Text style={styles.titulo}>Disponibilidad horaria</Text>
            <ScrollView style={{flexDirection: 'column',
                    backgroundColor: 'transparent',height:490}}>
            <FlatList 
                    
                    data={this.props.items}
                    //dir={this.state.urls}
                    //count={this.state.count}
                    renderItem={
                        
                        ({item})=>
                        {
                            if (item.day==1)
                                item.day="Lunes"
                            else if(item.day==2)
                                item.day="Martes"
                            else if (item.day==3)
                                item.day="Miercoles"
                            else if(item.day==4)
                                item.day="Jueves"
                            else if (item.day==5)
                                item.day="Viernes"
                            else if (item.day==6)
                                item.day="Sabado"
                            else if(item.day==7)
                                item.day="Domingo"
                                
                                //var from = this.hourToString(item.fromHour)
                                //var to = this.hourToString(item.toHour)
                                //console.log(item)
                              //  for(let j=0; j < 1 ; j++ ){
                                
                                    //console.log("asd",item.scheduleAvailability[j]._id);
                            
                                //}    
                            
                            return(
                                <View style ={[styles.column,{borderWidth: 0,margin:3}]}>
                                    <View style={[styles.column]}>
                                        <Text style={{alignSelf:'center'}}>Título: {item.title}</Text>
                                        <Text style={{alignSelf:'center'}}>Ciudad: {item.city}</Text>
                                        <View style={{ flexDirection: 'row',justifyContent:'space-between' }}>
                                            <View style={{flexDirection:'column'}}>
                                                <Text style={{}}> Lu</Text>
                                                <CheckBox
                                                    value={this.props.lunes}
                                                    onValueChange={this.props.onChangeLu}
                                                />
                                                
                                            </View>
                                            <View style={{flexDirection:'column'}}>
                                                <Text style={{}}> Ma</Text>
                                                <CheckBox
                                                    value={this.props.martes}
                                                    onValueChange={this.props.onChangeMa}
                                                />
                                                
                                            </View>
                                            <View style={{flexDirection:'column'}}>
                                                <Text style={{}}> Mi</Text>
                                                <CheckBox
                                                    value={this.props.miercoles}
                                                    onValueChange={this.props.onChangeMi}
                                                />
                                            </View>
                                            <View style={{flexDirection:'column'}}>
                                                <Text style={{}}> Ju</Text>
                                                <CheckBox
                                                    value={this.props.jueves}
                                                    onValueChange={this.props.onChangeJu}
                                                />
                                            </View>
                                            <View style={{flexDirection:'column'}}>
                                                <Text style={{}}> Vi</Text>
                                                <CheckBox
                                                    value={this.props.viernes}
                                                    onValueChange={this.props.onChangeVi}
                                                />
                                            </View>
                                            <View style={{flexDirection:'column'}}>
                                                <Text style={{}}> Sa</Text>
                                                <CheckBox
                                                    value={this.props.sabado}
                                                    onValueChange={this.props.onChangeSa}
                                                />
                                            </View>
                                            <View style={{flexDirection:'column'}}>
                                                <Text style={{}}> Do</Text>
                                                <CheckBox
                                                    value={this.props.domingo}
                                                    onValueChange={this.props.onChangeDo}
                                                />
                                            </View>
                                        </View>
                                            
                                        
                                    </View>
                                    <View style={styles.row}>
                                        <View style={styles.column}>
                                            <Text>
                                                Inicio: 
                                            </Text>
                                            <Text >
                                                {"     "}
                                                Fecha: {item.scheduleAvailability[0].fromDate.split("T")[0].split("-")[2]+"-"
                                                        +item.scheduleAvailability[0].fromDate.split("T")[0].split("-")[1]+"-"
                                                        +item.scheduleAvailability[0].fromDate.split("T")[0].split("-")[0]
                                                        } {"   "}
                                                Hora: {item.scheduleAvailability[0].fromDate.split("T")[1].split(":")[0]+":"+item.scheduleAvailability[0].fromDate.split("T")[1].split(":")[1]}
                                            </Text>
                                            <Text>
                                                Termino:
                                            </Text>
                                            <Text>
                                                {"     "}
                                                Fecha: {item.scheduleAvailability[item.scheduleAvailability.length-1].toDate.split("T")[0].split("-")[2]+"-"
                                                        +item.scheduleAvailability[item.scheduleAvailability.length-1].toDate.split("T")[0].split("-")[1]+"-"
                                                        +item.scheduleAvailability[item.scheduleAvailability.length-1].toDate.split("T")[0].split("-")[0]
                                                        } {"   "}
                                                Hora: {item.scheduleAvailability[item.scheduleAvailability.length-1].toDate.split("T")[1].split(":")[0]+":"+item.scheduleAvailability[item.scheduleAvailability.length-1].toDate.split("T")[1].split(":")[1]}
                                            </Text>
                                        </View>
                                        <Switch
                                            onValueChange = {this.props.onChangeSwitch}
                                            value = {this.props.isChecked}
                                        />
                                        
                                        <TouchableOpacity
                                            style={{backgroundColor:'transparent',borderRadius:8,marginBottom:2}}
                                            onPress={()=>this.props.handleRemoveItem(item._id)}
                                        >
                                            <Image 
                                                style={{width: 33, height: 33,tintColor:'rgb(145, 26, 26)'}}
                                                source={require('./picture/delete.png')}
                                            />
                                        </TouchableOpacity>
                                    </View>
                                </View>
                            )
                        
                        }
                                    
                    }
                    keyExtractor={(item,index)=>index.toString()}
            />
            </ScrollView>

          
        </View>
        
      );
    }
  }
  
  
  const styles = StyleSheet.create({
    column:{
      //flex: 1,
      flexDirection:'column',
      justifyContent: 'space-between',
      //alignItems: 'center',
      //backgroundColor: 'red',
      backgroundColor:'transparent',
      //paddingVertical:2,
      backgroundColor:'#8CD2CC',
      //marginBottom:2,
      //borderWidth: 2
    },
    row: {
      flexDirection:'row',
      justifyContent: 'space-between',
      alignItems: 'center',
      //backgroundColor: 'red',
      backgroundColor:'transparent',
      paddingVertical:10,
      backgroundColor:'#8CD2CC',
      marginBottom:5,
    },
    titulo:{
      fontSize:23,
      color:'black',
      textAlign:'center',
      fontWeight: 'bold'
      //margin:10
    },
    list:{
      marginTop:10
    }
  });
  