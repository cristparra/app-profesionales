
# Aplicación bHelath para Profesionales

## Instalar react native
Para correr el proyecto es necesario tener instalado react-native, que a su vez debe tener instalado Node, la interfaz de línea de comandos de React Native, Python2, un [JDK](https://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html) y Android Studio.

Para instalar react native cli ejecutar el siguiente comando:

```
npm install -g react-native-cli
```
##Proyecto
Una vez hechos los pasos anteriores, para ejecutar el proyecyo debe instalar las dependencias usadas por lo que debe ejecutar el siguiente comando:

```
npm install
```

o instalarlas por separado:

```
npm install --save react-navigation
npm install moment --save
npm install --save react-native-calendars
npm install --save react-native-modal
npm install react-native-datepicker --save
npm install tcomb-form-native
```

Para compilar la aplicación y poder verla se debe tener conectado un celular con sistema operativo android mediante un usb al computador, obien tener corriendo un emulador de celular. Posteriormente se debe abrir una consola y situarse en el directorio del proyecto y ejecutar el siguiente comando:

```
react-native run-android
```
El proyecto se compilará y se ejecutará en su dispositivo

##Aplicación 
A continunación se muestran gif del funcionamiento de la aplicación:

Login:
![Login](https://bitbucket.org/cristparra/app-profesionales/raw/ebf87ce21d542c2aa79d026df4d473f60ecad67d/capturas/login%20profecionales.gif)

Registro:
![Registro](https://bitbucket.org/cristparra/app-profesionales/raw/ebf87ce21d542c2aa79d026df4d473f60ecad67d/capturas/registro%20profesional.gif)

Disponibilidad:
![Disponibilidad](https://bitbucket.org/cristparra/app-profesionales/raw/ebf87ce21d542c2aa79d026df4d473f60ecad67d/capturas/disponibilidad%20profesionales.gif)

Agenda:
![Agenda](https://bitbucket.org/cristparra/app-profesionales/raw/ebf87ce21d542c2aa79d026df4d473f60ecad67d/capturas/agenda%20profesionales.gif)

Cerrar sesión:
![cerrar sesion](https://bitbucket.org/cristparra/app-profesionales/raw/ebf87ce21d542c2aa79d026df4d473f60ecad67d/capturas/cerrar%20sesion%20profesionales.gif)