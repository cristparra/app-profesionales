import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  TextInput,
  TouchableOpacity,
  ListView,
  Switch,
  ScrollView
} from 'react-native';

export default class ItemAgendar extends Component {

  render() {
    return (
      
      <View style={[styles.column,{backgroundColor:'transparent',marginTop:25}]}> 
      <Text style={styles.titulo}>Disponibilidad horaria</Text>
      <ScrollView style={{flexDirection: 'column',
      backgroundColor: 'transparent',height:490}}>
        <ListView 
            style={styles.list}
            enableEmptySections
            dataSource={this.props.dataSource}
            renderRow={({key, ...value})=>{
              if (value.day==1)
                value.day="Lunes"
              else if(value.day==2)
                value.day="Martes"
              else if (value.day==3)
                value.day="Miercoles"
              else if(value.day==4)
                value.day="Jueves"
              else if (value.day==5)
                value.day="Viernes"
              else if (value.day==6)
                value.day="Sabado"
              else if(value.day==7)
                value.day="Domingo"
            
                return(
                  <View style ={[styles.column,{borderWidth: 0,margin:3}]}>
                    <View style={[styles.column]}>
                        <Text>{value.day}</Text>
                    </View>
                    <View style={styles.row}>
                        <Text>Inicio: {value.date}</Text>
                        <Text>Termino: {value.finishTime}</Text>
                        
                        <Switch
                          />
                        
                        <TouchableOpacity
                        style={{backgroundColor:'red',borderRadius:8,marginBottom:2}}
                        onPress={()=>this.props.handleRemoveItem(key)}>
                            <Text style={{textAlign:'center'}}>Borrar</Text>
                        </TouchableOpacity>
                    </View>
                  </View>
                )
            }}
        />
        </ScrollView>
        
      </View>
      
    );
  }
}


const styles = StyleSheet.create({
  column:{
    //flex: 1,
    flexDirection:'column',
    justifyContent: 'space-between',
    //alignItems: 'center',
    //backgroundColor: 'red',
    backgroundColor:'transparent',
    //paddingVertical:2,
    backgroundColor:'#2d8ead',
    //marginBottom:2,
    //borderWidth: 2
  },
  row: {
    flexDirection:'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    //backgroundColor: 'red',
    backgroundColor:'transparent',
    paddingVertical:10,
    backgroundColor:'#2d8ead',
    marginBottom:5,
  },
  titulo:{
    fontSize:23,
    color:'black',
    textAlign:'center',
    //margin:10
  },
  list:{
    marginTop:10
  }
});
