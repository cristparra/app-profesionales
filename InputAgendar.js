import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
  TextInput,
  TouchableHighlight,
  Picker
} from 'react-native';
import Modal from "react-native-modal";
import moment from 'moment'
import DatePicker from 'react-native-datepicker'



export default class InputAgendar extends Component {
    constructor(props){
        super(props)
        this.state = {
            title:''
          };
    }
    render(){
        return(
            <View style={styles.container}>
                    <Text style={styles.text}>Disponibilidad horaria</Text>
                    <TextInput
                        style={{height: 40, borderColor: 'black', borderWidth: 1}}
                        onChangeText={(title) => this.setState({title})}
                        value={this.state.title}
                        placeholder={"Ingrese un título"}
                    />
                    <Picker
                        //style={{justifyContent:'center'}}
                        //backgroundColor
                        //CAmbiar el color del fondo se puede solo a través de android nativo https://stackoverflow.com/questions/38921492/how-to-style-the-standard-react-native-android-picker/39141949#39141949
                        prompt='Seleccionar ciudad'
                        mode='dialog'
                        selectedValue={this.props.city}
                        onValueChange={itemValue => this.props.onChangeCity(itemValue)}>
                        {this.props.citys.map((i, index) => (
                        <Picker.Item key={index} color={'black'} label={i.label} value={i.label} 
                        
                        />
                        ))}
                        
                    </Picker>

                    <DatePicker
                        date={this.props.date}
                        style={[styles.input,]}
                        mode='datetime'
                        placeholder='Hora de inicio'
                        format= 'YYYY-MM-DD HH:mm'
                        //minDate='2018-10-07'
                        //maxDate='2018-12-13'
                        confirmBtnText='Confirm'
                        cancelBtnText='Cancel'
                        onDateChange={ (date) => this.props.onChangeDate(date) }
                        customStyles={{
                            placeholderText: {
                                color:'black'
                            },
                            dateTouchBody: {borderColor:"black",backgroundColor:'#696A69'}
                        }}
                    />
                    <DatePicker
                        date={this.props.finishTime}
                        style={[styles.input]}
                        mode='datetime'
                        placeholder='Hora de termino'
                        format='YYYY-MM-DD HH:mm'
                        confirmBtnText='Confirm'
                        cancelBtnText='Cancel'
                        onDateChange={(date) => this.props.onChangeFinishTime(date)}
                        customStyles={{
                            placeholderText: {
                                color:'black'
                            },
                            dateTouchBody: {borderColor:"black",backgroundColor:'#696A69'}
                        }}
                    />
                    <TouchableHighlight
                        style={[styles.buttonstyle]}
                        onPress={
                            //this.props.handleAddItems
                            this.props.sumFunc
                            //this.props.sendData 
                            }
                    >
                        <Text style={[{textAlign:'center',fontSize:22,color:'black'}]}>Agregar</Text>
                    </TouchableHighlight>
    
            </View>
        )    
    }
}
const styles =StyleSheet.create({
    container:{
        marginTop:40,
    },
    modalTop:{
        justifyContent:'flex-start',
        marginTop:50
    },
    input:{
        width:350,
        alignSelf:'center',
        marginBottom:5,
        borderColor:'black',
        borderWidth:1,
        borderRadius:3,
    },
    text:{
        textAlign:'center',
        fontSize:25,
        color:'black',
        //backgroundColor:'#2d8ead'
    },
    buttonstyle:{
        backgroundColor:'#8CD2CC',
        width:220,
        margin:8,
        alignSelf:'center',
        borderWidth: 2,
        borderRadius:10
    }
})